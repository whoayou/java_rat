/*    */ package options;
/*    */ 
/*    */ import java.awt.MouseInfo;
/*    */ import java.awt.Point;
/*    */ import java.awt.PointerInfo;
/*    */ import java.io.IOException;
/*    */ import java.io.ObjectOutputStream;
/*    */ 
/*    */ public class Estatus extends Thread
/*    */ {
/* 17 */   private int status = 0;
/*    */   private boolean conectado;
/* 19 */   private boolean notificadoOffline = false;
/* 20 */   private boolean notificadoOnline = false;
/*    */   private final ObjectOutputStream out;
/*    */ 
/*    */   public Estatus(ObjectOutputStream out)
/*    */   {
/* 24 */     this.out = out;
/* 25 */     this.conectado = true;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     do {
/* 31 */       int actual = MouseInfo.getPointerInfo().getLocation().x;
/* 32 */       if (actual == this.status) {
/* 33 */         if (!this.notificadoOffline) {
/* 34 */           sendEstado(2);
/* 35 */           this.notificadoOffline = true;
/* 36 */           this.notificadoOnline = false;
/*    */         }
/*    */       }
/*    */       else
/*    */       {
/* 41 */         this.status = actual;
/* 42 */         if (!this.notificadoOnline) {
/* 43 */           this.status = actual;
/* 44 */           sendEstado(1);
/* 45 */           this.notificadoOnline = true;
/* 46 */           this.notificadoOffline = false;
/*    */         }
/*    */       }
/*    */ 
/* 50 */       duerme();
/* 51 */     }while (this.conectado);
/*    */   }
/*    */ 
/*    */   private void duerme() {
/*    */     try {
/* 56 */       sleep(300000L);
/*    */     } catch (InterruptedException ex) {
/*    */     }
/*    */   }
/*    */ 
/*    */   private void sendEstado(int i) {
/* 62 */     synchronized (this.out) {
/*    */       try {
/* 64 */         this.out.writeInt(i);
/* 65 */         this.out.flush();
/*    */       } catch (IOException ex) {
/* 67 */         this.conectado = false;
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   public void para() {
/* 73 */     this.conectado = false;
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Estatus
 * JD-Core Version:    0.6.2
 */