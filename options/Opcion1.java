/*    */ package options;
/*    */ 
/*    */ import javax.swing.JOptionPane;
/*    */ 
/*    */ public class Opcion1 extends Thread
/*    */ {
/*    */   private final int tipo;
/*    */   private final int opcion;
/*    */   private final String titulo;
/*    */   private final String cuerpo;
/*    */ 
/*    */   public void run()
/*    */   {
/* 14 */     JOptionPane.showOptionDialog(null, this.cuerpo, this.titulo, this.opcion, this.tipo, null, null, null);
/*    */   }
/*    */   public Opcion1(int tipo, int opcion, String titulo, String cuerpo) {
/* 17 */     this.tipo = tipo;
/* 18 */     this.opcion = opcion;
/* 19 */     this.titulo = titulo;
/* 20 */     this.cuerpo = cuerpo;
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Opcion1
 * JD-Core Version:    0.6.2
 */