/*    */ package plugins;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.lang.reflect.InvocationTargetException;
/*    */ import java.lang.reflect.Method;
/*    */ import java.net.MalformedURLException;
/*    */ import java.net.URI;
/*    */ import java.net.URL;
/*    */ import java.net.URLClassLoader;
/*    */ 
/*    */ public class ModificadorClassPath
/*    */ {
/*    */   private static final String METODO_ADD_URL = "addURL";
/* 12 */   private static final Class[] PARAMETRO_METODO = { URL.class };
/*    */   private URLClassLoader loader;
/*    */   private Method metodoAdd;
/*    */ 
/*    */   public ModificadorClassPath()
/*    */   {
/*    */     try
/*    */     {
/* 17 */       this.loader = ((URLClassLoader)ClassLoader.getSystemClassLoader());
/* 18 */       this.metodoAdd = URLClassLoader.class.getDeclaredMethod("addURL", PARAMETRO_METODO);
/* 19 */       this.metodoAdd.setAccessible(true);
/*    */     } catch (NoSuchMethodException ex) {
/*    */     }
/*    */     catch (SecurityException ex) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public URL[] getURLs() {
/* 27 */     return this.loader.getURLs();
/*    */   }
/*    */   public void addURL(URL url) {
/* 30 */     if (url != null)
/*    */       try {
/* 32 */         this.metodoAdd.invoke(this.loader, new Object[] { url });
/*    */       } catch (IllegalAccessException ex) {
/*    */       } catch (IllegalArgumentException ex) {
/*    */       } catch (InvocationTargetException ex) {
/*    */       }
/*    */   }
/*    */ 
/*    */   public void addArchivo(File archivo) {
/* 40 */     if (archivo != null)
/*    */       try {
/* 42 */         addURL(archivo.toURI().toURL());
/*    */       }
/*    */       catch (MalformedURLException ex)
/*    */       {
/*    */       }
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     plugins.ModificadorClassPath
 * JD-Core Version:    0.6.2
 */