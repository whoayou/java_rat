/*    */ package plugins;
/*    */ 
/*    */ import extra.Constantes;
/*    */ import java.io.File;
/*    */ import java.io.FilenameFilter;
/*    */ import java.io.PrintStream;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Arrays;
/*    */ import java.util.Iterator;
/*    */ import java.util.Properties;
/*    */ import java.util.ServiceLoader;
/*    */ 
/*    */ public class CargadorPlugins
/*    */ {
/*    */   public boolean cargarPlugins()
/*    */   {
/* 24 */     boolean cargados = true;
/*    */     try {
/* 26 */       File[] jars = buscarPlugins();
/* 27 */       if (jars.length > 0) {
/* 28 */         ModificadorClassPath cp = new ModificadorClassPath();
/* 29 */         for (File jar : jars)
/* 30 */           cp.addArchivo(jar);
/*    */       }
/*    */     }
/*    */     catch (Exception ex) {
/* 34 */       cargados = false;
/* 35 */       System.err.println(ex.getMessage());
/*    */     }
/* 37 */     return cargados;
/*    */   }
/*    */   private static File[] buscarPlugins() {
/* 40 */     File pl = new File(System.getProperty("user.home") + "/." + Constantes.getProperty("pluginfoldername") + "/");
/* 41 */     if (!pl.exists()) {
/* 42 */       pl.mkdir();
/*    */     }
/* 44 */     ArrayList vUrls = new ArrayList();
/* 45 */     if ((pl.exists()) && (pl.isDirectory())) {
/* 46 */       File[] jars = pl.listFiles(new FilenameFilter()
/*    */       {
/*    */         public boolean accept(File dir, String name) {
/* 49 */           return name.endsWith("." + Constantes.attrs.getProperty("extensionname"));
/*    */         }
/*    */       });
/* 52 */       vUrls.addAll(Arrays.asList(jars));
/*    */     }
/* 54 */     return (File[])vUrls.toArray(new File[0]);
/*    */   }
/*    */   public UnrecomServer[] getPlugins() {
/* 57 */     ServiceLoader sl = ServiceLoader.load(UnrecomServer.class);
/* 58 */     sl.reload();
/* 59 */     ArrayList vAv = new ArrayList();
/* 60 */     for (Iterator it = sl.iterator(); it.hasNext(); )
/*    */       try {
/* 62 */         UnrecomServer pl = (UnrecomServer)it.next();
/* 63 */         vAv.add(pl);
/*    */       }
/*    */       catch (Exception ex) {
/*    */       }
/* 67 */     return (UnrecomServer[])vAv.toArray(new UnrecomServer[0]);
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     plugins.CargadorPlugins
 * JD-Core Version:    0.6.2
 */