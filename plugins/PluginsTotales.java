/*    */ package plugins;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class PluginsTotales
/*    */ {
/* 13 */   public static final HashMap<String, UnrecomServer> plugins = new HashMap();
/*    */ 
/*    */   public void loadPlugins() {
/* 16 */     plugins.clear();
/* 17 */     CargadorPlugins cargador = new CargadorPlugins();
/* 18 */     boolean cargados = cargador.cargarPlugins();
/* 19 */     if (cargados)
/*    */       try {
/* 21 */         UnrecomServer[] avisadores = cargador.getPlugins();
/* 22 */         if (avisadores.length > 0)
/* 23 */           for (final UnrecomServer a : avisadores) {
/* 24 */             plugins.put(a.getId(), a);
/* 25 */             new Thread(new Runnable()
/*    */             {
/*    */               public void run()
/*    */               {
/* 29 */                 a.offLine();
/*    */               }
/*    */             }).start();
/*    */           }
/*    */       }
/*    */       catch (Exception ex)
/*    */       {
/*    */       }
/*    */   }
/*    */ 
/*    */   public synchronized UnrecomServer getPluging(String idplugin)
/*    */   {
/* 42 */     return (UnrecomServer)plugins.get(idplugin);
/*    */   }
/*    */ 
/*    */   public synchronized int getSize()
/*    */   {
/* 47 */     return plugins.size();
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     plugins.PluginsTotales
 * JD-Core Version:    0.6.2
 */